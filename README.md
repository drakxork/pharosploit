# Pharosploit

* Pharosploit is python automation tool to help users to exploit a system faster :)
* 
## How to use The Tool :
* git clone https://gitlab.com/drakxork/pharosploit.git
* cd pharosploit
* chmod +x *
* pip3 install -r requirements.txt
* python3 pharosploit.py
## Pharosploit :
https://imgur.com/a/QXpbfTw
## Payload Genrator :
https://imgur.com/a/OhxUiAW
## Nmap Scan :
https://imgur.com/a/OhxUiAW
## SSH BruteForce :
https://imgur.com/a/OhxUiAW
# Connect with me:
* Instagram : 
https://www.instagram.com/drakxork/

* enjoy 😉
